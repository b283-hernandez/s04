-- Find all artists that has letter d in its name.
SELECT * FROM artists WHERE name LIKE "%d%";

-- Find all songs that has a length of less tan 3:50
SELECT *  FROM songs WHERE length < "00:03:50";

--Join the albums and songs tables. (only show the album name, song_name, and song length)
SELECT albums.album_title, song.song_name, song.length FROM albums
    JOIN songs ON albums.id = songs.album_id;

-- Join the artists and albums tables (Find albums that has letter a in its name)
SELECT * FROM artists
    JOIN albums ON albums.artist_id = artists.id
    WHERE album_title LIKE "%a%";

-- SORT the albums in Z-A Order show only the first 4 records
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- Join the albums and songs table sort albums from Z-A
SELECT * FROM albums
    JOIN songs ON albums.id = songs.album_id
    ORDER BY album_title DESC;