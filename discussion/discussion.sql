-- [SECTION] Add New Records

-- Add 5 artists, 2 albums each, 2 songs per album.

-- Add 5 artists.

INSERT INTO artists (name) VALUES ("Taylor Swift");
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");

-- Taylor Swift

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Fearless", "2008-11-11", 3);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Fearless", "00:04:06", "Pop rock", 3);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Love Story", "00:03:33", "Country pop", 3);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Red", "2012-10-22", 3);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("State of Grace", "00:04:33", "Rock, alternative rock, arena rock", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Red", "00:03:24", "Country", 4);

-- Lady Gaga

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("A Star Is Born", "2018-10-05", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Black Eyes", "00:03:01", "Rock and roll", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Shallow", "00:03:21", "Country, rock, folk rock", 5);	

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Born This Way", "2011-05-23", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Born This Way", "00:04:12", "Electropop", 6);

-- Justin Bieber

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Purpose", "2015-11-13", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Sorry", "00:03:12", "Dancehall-poptropical housemoombahton", 7);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Believe", "2012-06-15", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Boyfriend", "00:04:11", "Pop", 8);

-- Ariana Grande

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Dangerous Woman", "2016-05-20", 6);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Into You", "00:04:02", "EDM house", 9);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Thank U, Next", "2019-02-08", 6);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Thank U Next", "00:03:16", "Pop, R&B", 10);

-- Bruno Mars

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("24K Magic", "2016-11-18", 7);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("24K Magic", "00:03:27", "Funk, disco, R&B", 11);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Earth to Mars", "2011-09-11", 7);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Lost", "00:03:12", "Pop", 12);

-- [SECTION] Advanced SELECT

-- Display the title and genre from all the songs;
SELECT song_name, genre FROM songs;

-- Exclude records
SELECT * FROM songs WHERE id != 11 AND id != 13;

-- greater than or less than
SELECT * FROM songs WHERE id < 11;
SELECT * FROM songs WHERE id > 11;

-- greater than or equal OR less than or equal
SELECT * FROM songs WHERE id <= 11;
SELECT * FROM songs WHERE id >= 11;

-- Get the specific IDs (OR)
SELECT * FROM songs WHERE id = 2 OR id = 3 OR id = 5;

-- Get the specific IDs IN
SELECT * FROM songs WHERE id IN (2, 3, 5);
SELECT * FROM songs WHERE genre IN ("Pop", "Kpop");

-- combining conditions
SELECT * FROM songs WHERE album_id = 4 AND id < 8;

-- Finding Partial Matches.

-- The "%" symbol and underscore (_);
-- % - represents all the characters that may exist  before or after the given characters, after the LIKE function (keyword)
-- _ - represents all the characters that may exist before or after the given characters, after LIKE function

SELECT * FROM songs WHERE song_name LIKE "%a";
-- select keywords from the end.
SELECT * FROM songs WHERE song_name LIKE "bo%";
-- select keywords from the start.
SELECT * FROM songs WHERE song_name LIKE "%a%";
-- select keywords in between.

SELECT * FROM songs WHERE song_name LIKE "__d";
SELECT * FROM songs WHERE song_name LIKE "so___";

--
SELECT * FROM songs WHERE song_name LIKE "%_t_";
SELECT * FROM songs WHERE song_name LIKE "_o_%";

-- Sorting records
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;

-- old: it will return all genre from songs table
SELECT genre FROM songs;

-- New: will return all genre once without any duplicate
SELECT DISTINCT genre FROM songs;

-- [SECTION] Table joins

-- Combining 2 or more tables into one

SELECT * FROM artists
    JOIN albums ON artists.id = albums.artist_id;


-- combine more than 2 tables
SELECT * FROM artists
    JOIN albums ON artists.id = albums.artist_id
    JOIN songs ON albums.id = songs.album_id
    WHERE genre = "Pop"; -- with where clause


SELECT artists.name, albums.album_title FROM artists
    JOIN albums ON artists.id = albums.artist_id;

SELECT * FROM artists 
    LEFT JOIN albums ON artists.id = albums.artist_id;